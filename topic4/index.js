const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const port = 4000
const passport = require('./lib/passport')
const router = require('./router')

app.use(express.urlencoded({extended : true}))

app.use(session({
    secret : 'ini secret',
    resave : false,
    saveUninitialized : false
}))


app.use(passport.initialize())
app.use(passport.session())


app.use(flash())

app.set('view engine', 'ejs')
app.use(router)
app.listen(port, () => {
    console.log(port)
})

