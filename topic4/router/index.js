const router = require('express').Router()
const passport = require('../lib/passport')
const jwt = require('../lib/jwtPassport')
const jsonwebtoken = require('jsonwebtoken')
router.post('/login', passport.authenticate('local', {
    successRedirect : '/',
    failureRedirect : '/login',
    failureFlash : true
}), (req,res) => {
    console.log(req.body)
    return res.send('success')
})


router.get('/', jwt.authenticate('jwt', {session : false}), (req,res) => {
    console.log(req.body)
    res.send('uscscsa')
})

router.get('/login', (req,res) => {
    console.log(req.body)
    let token = jsonwebtoken.sign({foo: 'bar'}, '123123')

    res.send(token)
})

router.get('/', (req,res) => {
    console.log(req.session)
    res.send("hhell")
})

router.get('/logout', (req,res) => {
    req.session
})


module.exports = router