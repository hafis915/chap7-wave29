const passport = require('passport')
const passportJWT = require('passport-jwt')
const JWTStrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt
const user = require('../models/index.json').user

passport.use(new JWTStrategy({
    jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey : '123123'
}, (jwtPayload , cb) => {
    console.log(jwtPayload, "<< ini payload")
    return (cb(null, user[0]))
}))



module.exports = passport