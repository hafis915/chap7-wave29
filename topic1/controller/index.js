const data = require("../models/index.json")

async function getAllData(req,res) {
    try {
        res.status(200).json({
            data
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg : error
        })
    }
}

async function getOneData(req,res) {
    try {
        const index = req.params.index
        const _data = data[index]
        res.status(200).json({
            data: _data
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg : error
        })
    }
}


module.exports = {
    getAllData, getOneData
}



