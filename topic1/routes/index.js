const route = require('express').Router()
const { getAllData, getOneData }  = require("../controller")


route.get('/', getAllData)
route.get('/:index', getOneData)


module.exports = route