/**
 Async
  - Definisi v
  - Callback 
    - Urutan eksekusi v
    - Set time Out v
    - Set Interval
  - Promise
    - sintax
    - urutan eksekusi
  - Async Await
    - sintax
 */


function namaFungsi(parameter) {
    // code yang akan di eksekusi
    console.log('Literal')    

}

const namaFungsi2 =  () => {
    console.log('arrow')

}

function callback() {
    //logic
}


// callback( setTimeout(() => console.log('-----'), 2000)  )


function promise1( parameter1 ) {
    return new Promise((resolve,reject) => {
        if(parameter1 > 5) {
            resolve(true)
        }else {
            reject(false)
        }
    })
}

// promise1(6)
// .then(res => {
//     promise1(6)
//     .then(res => {
//         console.log('===== ')
//         promise1(8)
//         .then(res => {
//         console.log('===== ')
//             promise1(8)
//             .then(res => {

//             })
//             .catch(err => {

//             })
//         })
//         .catch(err => {

//         })
//     })
//     .catch(err => {

//     })
// })
// .catch(err => {
//     console.log(err)
// })


// promise1(6)
// .then(res => {
//     let data = res

//     if(data) {
//         console.log('==== ini true ===')
//     }
//     return promise1(6)
// })
// .then(res => {
//     console.log(res, "<< promise ke 2")
//     return promise1(7)
// })
// .then(res => {
//     console.log(res, "<< promise ke 3")
// })
// .catch(err => {
//     console.log(err)
// })

async function promise2(param) {
    try {
        let p1 = await promise1(7)
        let p2  = await promise1(8)
        let p3  = await promise1(8)
        let p4  = await promise1(8)
        let p5  = await promise1(3)

        if(p5) {
            return true
        }
    } catch (error) {
        throw error
    }   
}

let arr = [1,2,4,5,6,7,8]

arr.forEach(async (el, index) => {
    try {
        
    let data = await promise1(7)
    console.log(data)
    console.log(el, "<< ")
    } catch (error) {
        
    }

})

// promise2(4)
// .then(res => {
//     console.log(res, "<< ini res")
// })
// .catch(err => {
//     console.log(err, "<< ini error")
// })
