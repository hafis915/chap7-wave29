const jwt = require('jsonwebtoken')

class JWTHelpers {
    

    static async createToken(payload) {
        try {
            const token = await jwt.sign(payload, process.env.privateKey)
            return token
        } catch (error) {
            throw error
        }
    } 

    static async decryptToken(token) {
        try {
            const payload = await jwt.verify(token , process.env.privateKey)
            return payload
        } catch (error) {
            throw false;
        }
    }
}

module.exports = JWTHelpers