const router = require('express').Router()
const viewRouter = require("./view.router")
const modelRouter = require('./model.router')


router.use('/view', viewRouter)
router.use('/', modelRouter)


module.exports = router