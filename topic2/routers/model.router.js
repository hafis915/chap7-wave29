const router = require('express').Router()
const { modelController } = require("../controllers")
const {authentication, authorization} = require('../middleware')


router.post('/login', modelController.login)
router.get('/product', authentication ,modelController.getProducts)
router.get('/user/:id', [authentication, authorization], modelController.getUser)


module.exports = router