const router = require('express').Router()
const { viewController } = require('../controllers')


router.get('/', viewController.home)



module.exports = router