const productData = require('../models/index.json').products


async function home(req,res) {
    try {
        console.log(productData)
        return res.render('./index', {data : productData, isLogin : false})
    } catch (error) {
        res.status(400).json({
            error : error.message
        })
    }
}

async function about(req,res) {

}



module.exports = {
    home, about
}