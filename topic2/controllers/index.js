const { home, about } = require('./view.controller')
const { login, getProducts , getUser} = require('./model.controller')


const viewController = { home, about }
const modelController = { login, getProducts, getUser }



module.exports = {
    viewController,
    modelController
}
