const user = require('../models/index.json').user
const products = require('../models/index.json').products
const { JWTHelpers } = require('../helpers')


async function login(req,res) {
    try {
        const users = user[0]
        const {userName, password} = users
        console.log(req.body)
        const { userName : userNameReq, password : passwordReq } = req.body
        
        if(password === passwordReq) {
            const payload = {
                userName, password, id : 1
            }
            const token = await JWTHelpers.createToken(payload)
            console.log(token)
            return res.status(200).json({
                token 
            })
        }else {
            return res.status(401).json({
                error : "Password atau userName salah"
            })
        }


    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg : error.message || error
        })
    }
}

async function getProducts(req,res) {
    try {
        const _products = products
        return res.status(200).json({
            data : _products
        })
    } catch (error) {
        res.status(500).json({
            msg : error.message || error
        })
    }
}

async function getUser(req,res) {
    try {
        const users = user[0]
        return res.status(200).json({
            data : users
        })
    } catch (error) {
        res.status(500).json({
            msg : error.message || error
        })
    }
}


module.exports = {
    login,
    getProducts,
    getUser
}