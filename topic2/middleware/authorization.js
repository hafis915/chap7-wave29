const { JWTHelpers } = require('../helpers')

async function authorization(req,res,next){
    try {
        const token = req.headers.access_token
        const payload = await JWTHelpers.decryptToken(token)
        
        if(payload) {
            const id = req.params.id
            //check id yang mau di edit di bandingkan dengan id yang ada di token
            if(+id === +payload.id) {
                next()
            }else {
                throw ''
            }
        }else {
            throw ''
        }
    } catch (error) {
        res.status(401).json({
            msg : 'Unathorized'
        })
    }
}

module.exports = authorization