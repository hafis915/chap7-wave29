const { JWTHelpers } = require('../helpers')

async function authentication(req,res, next) {
    try {
        const token = req.headers.access_token
        console.log(token)
        const payload = await JWTHelpers.decryptToken(token)
        if(payload) {
            // find one user 
            console.log(payload)
            next()
        }else {
            return res.status(401).json({
                msg : 'Please Login'
            })
        }
    } catch (error) {
        res.status(401).json({
            msg : 'Please Login'
        })
    }
}

module.exports = authentication