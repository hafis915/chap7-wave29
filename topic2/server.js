require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.port || 4000
const routers = require('./routers')


app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.set('view engine', 'ejs')
app.use(routers)

app.listen(port, () => {
    console.log(port)
})