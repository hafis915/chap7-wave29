const router = require('express').Router()
const {loginPage, userPage, homePage, loginAPI , getProducts} = require('../controller')
const { passport } = require('../helper')
const {authent, author, jwtPassport} = require('../middleware')


router.get('/login', loginPage )
router.post('/login', loginAPI)
// router.post('/login', passport.authenticate('local', {
//     successRedirect : '/',
//     failureRedirect : '/login'
// }))
router.get('/', [authent] , homePage)
router.get('/user/:id',[authent, author], userPage)
router.get('/product', jwtPassport.authenticate('jwt', {session :false}) ,getProducts)

// router.get('/user/:id',[authent, author], userPage)


module.exports = router