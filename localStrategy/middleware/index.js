const passport = require('passport')
const passportJWT = require('passport-jwt')
const JWTSTrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt

passport.use(new JWTSTrategy({
    jwtFromRequest : ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey : '123123'
}, (user, done) => {

    console.log(user, "<< ini adalah payload")
    return done(null, user)

}))


async function authent(req,res,next) {
    try {
        let session = req.session.passport
        console.log(session)
        if(session) {
            next()
        }else {
            res.redirect('/login')
        }
    } catch (error) {
        res.send(error)
    }
}

async function author(req,res,next) {
    try {
        let session = req.session.passport
        let id = session.user
        let paramsId = req.params.id
        if(+id === +paramsId) {
            next()
        }else {
            res.redirect('/')
        }

    } catch (error) {
        
    }
}

module.exports = {authent, author, jwtPassport : passport}