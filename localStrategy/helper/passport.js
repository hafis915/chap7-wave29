const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const user = require('../models/index.json').user


passport.use(new LocalStrategy(async (username, password, done) => {
    console.log(username, password, "<< Ini username dan password")
    const {userName : usernameData, password : passwordData, id} = user[0]

    if(usernameData === username && password === passwordData ) {
        done(null, user[0])
    }else {
        done(null , null)
    }
}))
passport.serializeUser((user, done) => {
    done(null, user.id)
} )
passport.deserializeUser(function(user, done) { //Here you retrieve all the info of the user from the session storage using the user id stored in the session earlier using serialize user.
    done(null,{id : user})
});

module.exports = passport



